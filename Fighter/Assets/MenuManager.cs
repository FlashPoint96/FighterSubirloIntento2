﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    // Start is called before the first frame update
    public void Mapa1()
    {
        SceneManager.LoadScene("Mapa1");
    }
    public void Mapa2()
    {
        SceneManager.LoadScene("Mapa2");
    }

    public void Mapa3()
    {
        SceneManager.LoadScene("SampleScene");
    }

}
