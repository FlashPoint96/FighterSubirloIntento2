﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
public class ApagarLuce : MonoBehaviour
{
    //Esto no se porque lo he hecho la verdad. Pero es una luz que se apage y se enchufa.
    public void Start()
    {
        InvokeRepeating("Enchufa", 0f, 5f);
        InvokeRepeating("ApagarLu", 0f, 15f);
    }

    void ApagarLu()
    {
         this.GetComponent<Light2D>().intensity = 0;
    }

    void Enchufa()
    {
        this.GetComponent<Light2D>().intensity = 1;
    }
}
