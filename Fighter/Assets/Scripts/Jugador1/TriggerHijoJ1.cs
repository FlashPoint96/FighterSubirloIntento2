﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHijoJ1 : MonoBehaviour
{
    //al entrar en trigger llamada a la funcion del padre con la collision y este objeto para que el padre pueda saber que hijo ha sido afectado. 
   private void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.GetComponentInParent<HurtBoxJugador1>().PullTrigger(collision, this.gameObject);
    }
}
