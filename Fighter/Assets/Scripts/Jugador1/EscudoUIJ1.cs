﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscudoUIJ1 : MonoBehaviour
{
    private GameObject J1;
    private bool escudo;
    // Start is called before the first frame update
    void Start()
    {
        J1 = GameObject.Find("Jugador");
    }

    //Mira si el jugador tiene escudo que pueda usar y si tiene la imagen esta activa.
    // Update is called once per frame
    void Update()
    {
        escudo = J1.GetComponent<Jugador>().Defense;
        this.GetComponent<Image>().enabled = escudo;
    }
}
