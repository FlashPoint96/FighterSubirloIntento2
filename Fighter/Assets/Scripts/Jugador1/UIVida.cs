﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIVida : MonoBehaviour
{
    private GameObject J1;
    private float MaxVida = 100;
    private float VidaActual;
    // Start is called before the first frame update
    void Start()
    {
        J1 = GameObject.Find("Jugador");
    }

    // Update is called once per frame
    void Update()
    {
        VidaActual = J1.GetComponent<Jugador>().vida;
        this.GetComponent<Image>().fillAmount = VidaActual / MaxVida;
    }
}
