﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBoxJugador1 : MonoBehaviour
{
    private GameObject J1;
    public GameObject sangres; 
    public GameObject waters;
    //Estas 4 variabales las uso para hacer KO al usuario y que no se pueda mover durante unos segundos. Cuanto los golpes que ha dado y en cuantos segundos si los ha dado rapido y el otro jugador puede estar KO.Lo deja KO.
    public static int hits;
    public static float timebtw;
    public static float firsthit;
    private bool first;
    // Start is called before the first frame update
    void Start()
    {
        J1 = GameObject.Find("Jugador");
        first = true;
    }

    //Llamo esta funcion cuando un hijo de la hurtbox ha sido entrado por trigger.Entonces compruebo si el jugador se esta defendiendo ya que si se esta defiendo no puedo hacerle le dano por lo que le resto el tiempo que esta
    //defiendose y si no se esta defendiendo compruebo a que parte de la hurtbox le ha entrado el trigger para saber cuanto dano hacer y luego le empuja y instancio la sangre.
    public void PullTrigger(Collider2D collision, GameObject GO)
    {
        if (collision.gameObject.tag == "Plataformas")
        {
            if (collision.gameObject.name == "agua")
            {
                Instantiate(waters, this.transform.position, Quaternion.identity);
            }
        }
        else
        {
            if (first)
            {
                firsthit += Time.deltaTime;
                first = false;
            }
            if (J1.GetComponent<Jugador>().Defendiendo)
            {
                J1.GetComponent<Jugador>().DefenseUse -= Time.deltaTime;
                StartCoroutine(Empujar());

            }
            else
            {
                StartCoroutine(Empujar());
                if (GO.gameObject.name.Equals("Cabeza"))
                {
                    J1.GetComponent<Jugador>().DamageThis(5);
                }
                else if (GO.gameObject.name.Equals("Torso"))
                {
                    J1.GetComponent<Jugador>().DamageThis(1);
                }
                else if (GO.gameObject.name.Equals("Brazo1"))
                {
                    J1.GetComponent<Jugador>().DamageThis(1);
                }
                else if (GO.gameObject.name.Equals("Brazo2"))
                {
                    J1.GetComponent<Jugador>().DamageThis(1);
                }
                else if (GO.gameObject.name.Equals("Pierna1"))
                {
                    J1.GetComponent<Jugador>().DamageThis(1);
                }
                else if (GO.gameObject.name.Equals("Pierna2"))
                {
                    J1.GetComponent<Jugador>().DamageThis(1);
                }
                timebtw += Time.deltaTime;
                if (hits >= 7)
                {
                    if (firsthit + 0.3f > timebtw)
                    {
                        if (J1.GetComponent<Jugador>().CanGetKo)
                        {
                            J1.GetComponent<Jugador>().KO = true;
                            J1.GetComponent<Jugador>().CanGetKo = false;
                        }
                    }
                    first = true;
                    firsthit = 0f;
                    timebtw = 0f;
                    hits = 0;
                }
                Instantiate(sangres, this.transform.position, Quaternion.identity);
            }
        }
    }
   
    //Le anado una fuerza de impulso al J2 que este personaje.
    IEnumerator Empujar()
    {
        if (J1.GetComponent<Jugador>().dondemira)
        {
            J1.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0.3f, 0), ForceMode2D.Impulse);
            yield return new WaitForSeconds(0.2f);
        }
        else if (!J1.GetComponent<Jugador>().dondemira)
        {
            J1.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-0.3f, 0), ForceMode2D.Impulse);
            yield return new WaitForSeconds(0.2f);
        }
        //J2.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-30, 0));
    }
}
