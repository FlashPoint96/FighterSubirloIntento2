﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    //Animator
    public Animator animator;
    //Bool de si se esta defendiendo
    public bool Defendiendo;
    //Vida
    public float vida = 100;
    //El Hadouken
    public GameObject HK;
    //Cosas de KO
    public bool KO = false;
    private float KoUse = 4f;
    public bool CanGetKo = true;
    //RigiBody Velocidad y estados de lso combos
    private Rigidbody2D RigiBody;
    private int velocidad = 2;
    private int ComboStatus = 0;
    private int HadoukenStatus = 0;
    private int Combo2Status = 0;
    //Todos los CDS y el tiempo que puede usar la defensa
    private float DefenseCD;
    private float ComboBaseCD;
    private float ComboBase2CD;
    private float HadoukenCD;
    public float DefenseUse = 5f;
    //Rotacion es para saber donde poner al personaje.
    private float rotacion;
    //Saltar
    private bool salto;
    //Aqui pone al personaje a un lado o al otro
    public bool dondemira;
    //Mira si los combos estan en CD o se puede usar
    public bool Defense;
    private bool Hadouken;
    private bool ComboBase;
    private bool ComboBase2;


    private float TimeBTW;
    private bool CanGetHit;

    // Start is called before the first frame update
    void Start()
    {
        RigiBody = this.GetComponent<Rigidbody2D>();
        Defense = true;
        Hadouken = true;
        ComboBase = true;
        ComboBase2 = true;
        Defendiendo = false;
        dondemira = false;
        TimeBTW = 0f;
        CanGetHit = true; 
        KO = false;
        CanGetKo = true;
        KoUse = 4f;
    }

    // Update is called once per frame
    void Update()
    {
        if (KO)
        {

        }
        else
        {
            Moverse();
        }
        VerCDs();
    }

    public void TodoslosBooleansJ1()
    {
        vida = 100;
        Defense = true;
        Hadouken = true;
        ComboBase = true;
        ComboBase2 = true;
        Defendiendo = false;
        dondemira = false;
        TimeBTW = 0f;
        CanGetHit = true; 
        KO = false;
        CanGetKo = true;
        KoUse = 4f;
    }

    //Miro todos los CDs.
    private void VerCDs()
    {
        if (!Defense)
        {
            DefenseCD += Time.deltaTime;
            if (DefenseCD >= 5)
            {
                Defense = true;
                DefenseCD = 0;
                DefenseUse = 5f;
            }
        }
        if (!Hadouken)
        {
            HadoukenCD += Time.deltaTime;
            if (HadoukenCD >= 8)
            {
                Hadouken = true;
                HadoukenCD = 0;
            }
        }
        if (!ComboBase)
        {
            ComboBaseCD += Time.deltaTime;
            if (ComboBaseCD >= 4)
            {
                ComboBase = true;
                ComboBaseCD = 0;
            }
        }
        if (!ComboBase2)
        {
            ComboBase2CD += Time.deltaTime;
            if (ComboBase2CD >= 5)
            {
                ComboBase2 = true;
                ComboBase2CD = 0;
            }
        }
        if (!CanGetHit)
        {
            TimeBTW += Time.deltaTime;
            if (TimeBTW >= 0.05f)
            {
                CanGetHit = true;
                TimeBTW = 0f;
            }
        }
        if (KO)
        {
            KoUse -= Time.deltaTime;
            if (KoUse <= 0)
            {
                KO = false;
            }
        }
    }

    //Moverse. 
    private void Moverse()
    {
        if (Input.GetKey("d"))
        {
            RigiBody.velocity = new Vector2(velocidad, RigiBody.velocity.y);
            rotacion = 0;
            dondemira = false;
        }
        else if (Input.GetKey("a"))
        {
            dondemira = true;
            rotacion = 180;
            RigiBody.velocity = new Vector2(-velocidad, RigiBody.velocity.y);
        }
        if (dondemira)
        {
            CambiarPosicion(rotacion);
        }
        else
        {
            CambiarPosicion(rotacion);
        }


        if (Input.GetKeyDown("space") && salto)
        {
            RigiBody.AddForce(new Vector2(0, 200));
            salto = false;
        }

        Pegar();
    }
    //Cambia la posicion del personaje
    private void CambiarPosicion(float rotacion)
    {
        this.transform.eulerAngles = new Vector3(
        this.transform.eulerAngles.x,
        rotacion,
        this.transform.eulerAngles.z);
    }
    //Aqui llamo a las corutinas de pegar.
    private void Pegar()
    {
        if (Input.GetKeyDown("f"))
        {
            StartCoroutine(Pegar("f"));
        }
        else if (Input.GetKeyDown("g"))
        {
            StartCoroutine(Pegar("g"));
        }

        if (Input.GetKeyDown("v"))
        {
            StartCoroutine(Combo2("v"));
        }else if (Input.GetKeyDown("c"))
        {
            StartCoroutine(Combo2("c"));
        }
        else if (Input.GetKeyDown("z"))
        {
            StartCoroutine(Combo2("z"));
        }



        if (Input.GetKeyDown("h") && Hadouken)
        {
            StartCoroutine(HadoukenCombo("h", 0.3f));
        }
        else if (Input.GetKeyDown("j") && Hadouken)
        {
            StartCoroutine(HadoukenCombo("j", 0.2f));
        }else if(Input.GetKeyDown("k") && Hadouken)
        {
            StartCoroutine(HadoukenCombo("k", 0f));
        }else if(Input.GetKeyDown("h") || Input.GetKeyDown("j") || Input.GetKeyDown("k") && !Hadouken)
        {
            print("Tienes el Hadouken EN CD");
        }

        if (Input.GetKey("l") && Defense)
        {
            DefenseUse -= Time.deltaTime;
            if (DefenseUse <= 0){
                Defense = false;
                DefenseUse = 0;
                Defendiendo = false;
                animator.SetBool("Bloqueo", false);
            }else if (DefenseUse > 0)
            {
                animator.SetBool("Bloqueo", true);
                Defendiendo = true;
            }
        }else if(!Input.GetKey("l"))
        {
            Defendiendo = false;
            animator.SetBool("Bloqueo", false);
        }
    }


    //Combo V2 es el combo.
    IEnumerator Combo2(String v)
    {
        if (v == "v")
        {
            if (Combo2Status==0)
            {
                Combo2Status = 1;
                yield return new WaitForSeconds(0.2f);
                if (Combo2Status == 1)
                {
                    //Combo Fallido                    
                    SoundManager.PonerSonido("Kick");
                    animator.SetBool("Patada", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Patada", false);
                    print("Combo Fallido");
                    Combo2Status = 0;
                }
            }
        }else if (v == "c")
        {
            if (Combo2Status == 1)
            {
                Combo2Status = 2;
                yield return new WaitForSeconds(0.2f);
                if (Combo2Status == 2)
                {
                    //Combo Fallido
                    animator.SetBool("Puno", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Puno", false);
                    print("Combo Fallido 2");
                    Combo2Status = 0;
                }
            }
        }
        else if (v == "z")
        {
            if (Combo2Status == 2)
            {
                Combo2Status = 3;
                if (Combo2Status == 3 && ComboBase2)
                {
                    SoundManager.PonerSonido("RyuCombo1");
                    animator.SetBool("Combo2", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Combo2", false);
                    //Combo Cumpldio
                    print("Combo Complido");
                    Combo2Status = 0;
                    ComboBase2 = false;
                }
                else
                {
                    print("Combo Cumplido pero no esta en CD");
                    Combo2Status = 0;
                }
            }
        }
    }
    //Esto no lo uso
    IEnumerator Protegerse()
    {
        animator.SetBool("Bloqueo", true);
        Defendiendo = true;
        yield return new WaitForSeconds(0f);
    }
    //Este es el combo de hadouken que esta controlado por el tiempo y mira si tiene el hadouken en CD. Si lo tiene lanza un hadouken.
    IEnumerator HadoukenCombo(string v, float wait)
    {
        if (v == "h")
        {
            if (HadoukenStatus == 0)
            {
                HadoukenStatus = 1;
                yield return new WaitForSeconds(wait);
                if (HadoukenStatus == 1)
                {
                    //Pegar algo Normal
                    SoundManager.PonerSonido("Kick");
                    animator.SetBool("Patada", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Patada", false);
                    HadoukenStatus = 0;
                }
            }
        }else if (v == "j")
        {
            if (HadoukenStatus == 1)
            {
                HadoukenStatus = 2;
                yield return new WaitForSeconds(wait);
                if (HadoukenStatus == 2)
                {
                    SoundManager.PonerSonido("RyuPuno");
                    //Pegar algo Normal y/o ver si el ComboBase esta en CD o si se pueda usar
                    animator.SetBool("Puno", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Puno", false);
                    HadoukenStatus = 0;
                }
            }
        }else if (v == "k")
        {
            if (HadoukenStatus == 2)
            {
                HadoukenStatus = 3;
                if (HadoukenStatus == 3)
                {
                    animator.SetBool("HadoukenCombo", true);
                    yield return new WaitForSeconds(0.5f);
                    CrearHadouken();
                    animator.SetBool("HadoukenCombo", false);
                    print("Hadouken COmpletado");
                    HadoukenStatus = 0;
                    Hadouken = false;
                }
            }
        }
    }

    //Aqui creo el Hadouken que se va a lanzar dependiendo de donde mire.
    private void CrearHadouken()
    {
        SoundManager.PonerSonido("Hadouken");
        if (dondemira)
        {
            Vector2 position2 = new Vector2(this.transform.position.x - 2, this.transform.position.y);
            GameObject aux = (GameObject)Instantiate(HK);
            aux.transform.position = position2;
            aux.transform.eulerAngles = new Vector3(
            aux.transform.eulerAngles.x,
            rotacion,
            this.transform.eulerAngles.z);
        }
        else
        {
            Vector2 position2 = new Vector2(this.transform.position.x + 2, this.transform.position.y);
            GameObject aux = (GameObject)Instantiate(HK);
            aux.transform.position = position2;
            aux.transform.eulerAngles = new Vector3(
            aux.transform.eulerAngles.x,
            rotacion,
            this.transform.eulerAngles.z);
        }
    }
    //Es cuando le pegan.
    IEnumerator Hit()
    {
        animator.SetBool("Hit", true);
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("Hit", false);
    }

    //Este la corutina basica de pegar solo le paso el string y mira si es una patada o un puno y si hace los dos a la vez hace un combo.
    IEnumerator Pegar(String v)
    {
        if (v == "f")
        {
            if (ComboStatus == 0)
            {
                ComboStatus = 1;
                yield return new WaitForSeconds(0.2f);
                if (ComboStatus == 1)
                {
                    SoundManager.PonerSonido("Kick");
                    animator.SetBool("Patada", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Patada", false);
                    ComboStatus = 0;
                }
            }
        } else if (v == "g")
        {
            if (ComboStatus == 0)
            {
                ComboStatus = 1;
                yield return new WaitForSeconds(0.2f);
                if (ComboStatus == 1)
                {
                    SoundManager.PonerSonido("RyuPuno");
                    animator.SetBool("Puno", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Puno", false);
                    //Pegar Puno
                    ComboStatus = 0;
                }
            }else if (ComboStatus == 1)
            {
                ComboStatus = 2;
                if (ComboStatus == 2 && ComboBase)
                {
                    SoundManager.PonerSonido("RyuCombo1");
                    //Pegar Combo
                    animator.SetBool("Combo1", true);
                    yield return new WaitForSeconds(1f);
                    animator.SetBool("Combo1", false);
                    ComboStatus = 0;
                    ComboBase = false;
                }
                else
                {
                    SoundManager.PonerSonido("RyuPuno");
                    animator.SetBool("Puno", true);
                    yield return new WaitForSeconds(0.5f);
                    animator.SetBool("Puno", false);
                    //Pegar Puno
                    print("Puno + Tienes el Combo en Cd");
                    ComboStatus = 0;
                }
            }
        }
        yield return new WaitForSeconds(0.8f);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Plataformas")
        {
            salto = true;
        }
    }
    //Resta la vida
    public void DamageThis(float i)
    {
        if (CanGetHit)
        {
            this.vida -= i;
            StartCoroutine(Hit()); 
            SoundManager.PonerSonido("RyuDamage");
            CanGetHit = false;
        }
    }
}
