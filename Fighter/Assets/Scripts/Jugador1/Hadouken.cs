﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hadouken : MonoBehaviour
{

    public float speed = 6f;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Destroy(this.gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector2.right * speed * Time.deltaTime);
        this.transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name=="Jugador2")
        {
            collision.gameObject.GetComponent<Jugador2>().DamageThis(10);
        }
        Destroy(this.gameObject);
    }

}
