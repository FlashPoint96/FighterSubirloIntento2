﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoriasJ1 : MonoBehaviour
{
    //Le paso un array de objetos que son las redonditas esas y las pinto en el update dependiendo de las victorisa que tenga.
    public int victorias;
    public GameObject[] V1;


    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < victorias; ++i)
        {
            V1[i].GetComponent<SpriteRenderer>().color = Color.red;
        }
    }
}
