﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscudoUIJ2 : MonoBehaviour
{
    private GameObject J2;
    private bool escudo;
    // Start is called before the first frame update
    void Start()
    {
        J2 = GameObject.Find("Jugador2");
    }

    // Update is called once per frame
    void Update()
    {
        escudo = J2.GetComponent<Jugador2>().Defense;
        this.GetComponent<Image>().enabled = escudo;
    }
}
