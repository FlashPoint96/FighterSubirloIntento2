﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaJugador2 : MonoBehaviour
{
    private GameObject J2;
    private float MaxVida = 100;
    private float VidaActual;
    // Start is called before the first frame update
    void Start()
    {
        J2 = GameObject.Find("Jugador2");
    }

    // Update is called once per frame
    void Update()
    {
        VidaActual = J2.GetComponent<Jugador2>().vida;
        this.GetComponent<Image>().fillAmount = VidaActual / MaxVida;
    }
}
