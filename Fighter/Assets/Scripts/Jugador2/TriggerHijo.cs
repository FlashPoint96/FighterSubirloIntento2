﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHijo : MonoBehaviour
{
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.GetComponentInParent<HurtBoxJugador2>().PullTrigger(collision, this.gameObject);
    }
}
