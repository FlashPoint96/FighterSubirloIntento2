﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueViento : MonoBehaviour
{
    public float speed = 6f;
    // Start is called before the first frame update
    void Start()
    {
       // GameObject.Destroy(this.gameObject, 2f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.localScale += new Vector3(0.1F, 0.1F, 0.1F); ;
        this.transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Jugador"){
            collision.gameObject.GetComponent<Jugador>().DamageThis(10);
        }
        Destroy(this.gameObject);
    }
}
