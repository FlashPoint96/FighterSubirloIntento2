﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiempoUI : MonoBehaviour
{
    GameObject gc;
    void Start()
    {
        gc = GameObject.Find("GameController");
        gc.GetComponent<GameController>().ActualitzarUIEvent += actualitzarUI;

    }

    private void actualitzarUI(float temps)
    {
        int tiempo = (int)temps;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = ""+tiempo;
    }
}
