﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    //Aqui tengo todos los sonidos usados en el juego.
    public static AudioClip HadoukenSonido, finalSonido, RyuKick, RyuDamaged, KenAtack1, KenAtack2, KenCombo, KenDamaged, RyuPuno, RyuCombo1;
    public static AudioClip fightSonido;
    static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        HadoukenSonido = Resources.Load<AudioClip>("Sonidos/Ryu/hk");
        RyuKick = Resources.Load<AudioClip>("Sonidos/Ryu/ryuken-kick");
        RyuDamaged = Resources.Load<AudioClip>("Sonidos/Ryu/ryuken-uggh");
        RyuPuno = Resources.Load<AudioClip>("Sonidos/Ryu/RyuPuno");
        RyuCombo1 = Resources.Load<AudioClip>("Sonidos/Ryu/ComboRyu1");

        KenAtack1 = Resources.Load<AudioClip>("Sonidos/Ken/1");
        KenDamaged = Resources.Load<AudioClip>("Sonidos/Ken/3");
        KenCombo = Resources.Load<AudioClip>("Sonidos/Ken/10");
        KenAtack2 = Resources.Load<AudioClip>("Sonidos/Ken/11");


        finalSonido = Resources.Load<AudioClip>("Sonidos/final");
        fightSonido = Resources.Load<AudioClip>("Sonidos/fight");
        audioSource = GetComponent<AudioSource>();
    }

    public static void PonerSonido(string sonido)
    {
        switch (sonido){
            case "Hadouken":
                audioSource.PlayOneShot(HadoukenSonido);
                break;
            case "Kick":
                audioSource.PlayOneShot(RyuKick);
                break;
            case "RyuDamage":
                //audioSource.PlayOneShot(RyuDamaged);
                //audioSource.volume = 0.2f;
                break;
            case "RyuPuno":
                audioSource.PlayOneShot(RyuPuno); 
                break;
            case "RyuCombo1":
                audioSource.PlayOneShot(RyuCombo1); 
                break;
            case "ATK1Ken":
                audioSource.PlayOneShot(KenAtack1); 
                break;
            case "ATK2Ken":
                audioSource.PlayOneShot(KenAtack2); 
                break;
            case "ComboKen":
                audioSource.PlayOneShot(KenCombo); 
                break;
            case "KenDamage":
                audioSource.PlayOneShot(KenDamaged); 
                break;
            case "Fight":
                audioSource.PlayOneShot(fightSonido); 
                break;
            case "Final":
                audioSource.PlayOneShot(finalSonido);
                break;
        }
        audioSource.volume = 0.5f;
    }
}
