﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Mapa1Invoke : MonoBehaviour
{
    //Esto en verdad es en el mapa 3 que use para hacer cosas.Invoco un barill cada x segundos.
    public GameObject barill;
    // Start is called before the first frame update   
    void Start()
    {
        InvokeRepeating("Barriles", 0f, 10f);
    }


    void Barriles()
    {
        GameObject instance = Instantiate(barill);
        instance.transform.position = new Vector2(Random.Range(-10.0f, 10.0f), 5f);
    }
}
