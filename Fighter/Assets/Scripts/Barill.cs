﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barill : MonoBehaviour
{
    //Es un barill que cae del cielo y tiene luz.
    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector2.down * 2f * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject.Destroy(this.gameObject);
    }
}
