﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    //Aqui tengo las cosas de J1 y sus cosas
    private GameObject J1;
    private GameObject J2;
    private GameObject CosasJ1;
    private GameObject CosasJ2;
    //Un delagado para los tiempo
    public delegate void TiempoUI(float temps);
    public event TiempoUI ActualitzarUIEvent;
    private float tiempo = 90;
    // Start is called before the first frame update
    void Start()
    {
        CosasJ1 = GameObject.Find("Victorias");
        CosasJ2 = GameObject.Find("Victorias2");
       // SoundManager.PonerSonido("Fight");
         J1 = GameObject.Find("Jugador");
        J2 = GameObject.Find("Jugador2");
    }
    //Invoco el evento. Miro que jugador a ganado a quien dependiendo de la vida si el tiempo a acabado.Y si no miro si hay alguno muerto.
    // Update is called once per frame
    void Update()
    {
        tiempo -= Time.deltaTime;

        if (ActualitzarUIEvent != null)
        {
            ActualitzarUIEvent.Invoke(tiempo);
        }
        if (tiempo <= 0)
        {
            if (J1.GetComponent<Jugador>().vida > J2.GetComponent<Jugador2>().vida)
            {
                CheckWinner(CosasJ1);
            }
            else if (J1.GetComponent<Jugador>().vida < J2.GetComponent<Jugador2>().vida)
            {
                CheckWinner(CosasJ2);
            }
            else if (J1.GetComponent<Jugador>().vida == J2.GetComponent<Jugador2>().vida)
            {
                tiempo = 15f;
            }
        }
        if (J1.GetComponent<Jugador>().vida <= 0)
        {
            CheckWinner(CosasJ2);
        }else if (J2.GetComponent<Jugador2>().vida <= 0)
        {
            CheckWinner(CosasJ1);
        }
        CheckGlobalWinner();
    }
    //Miro quien ha ganado 3 rondas pork ha ganado.
    private void CheckGlobalWinner()
    {
        if (CosasJ1.GetComponent<VictoriasJ1>().victorias == 3)
        {
            SoundManager.PonerSonido("Final");
            print("Gana j1");
            SceneManager.LoadScene("WinnerJ1");
        }
        else if (CosasJ2.GetComponent<VictoriasJ1>().victorias == 3)
        {
            SoundManager.PonerSonido("Final");
            print("Gana j2");
            SceneManager.LoadScene("WinnerJ2");
        }
    }
    //Miro que jugador ha ganado para subirle las victorias en el contador.
    private void CheckWinner(GameObject cosas)
    {
        if (cosas.GetComponent<VictoriasJ1>().victorias == 0)
        {
            cosas.GetComponent<VictoriasJ1>().victorias = 1;
            CrearTodoDeNuevo();
        }
        else if (cosas.GetComponent<VictoriasJ1>().victorias == 1)
        {
            cosas.GetComponent<VictoriasJ1>().victorias = 2;
            CrearTodoDeNuevo();
        }
        else if (cosas.GetComponent<VictoriasJ1>().victorias == 2)
        {
            cosas.GetComponent<VictoriasJ1>().victorias = 3;
            CrearTodoDeNuevo();
        }
    }
    //Despues de que uno gane un round.Pongo al personaje en la posicion inicial y pongo sus variables a 0.
    void CrearTodoDeNuevo()
        {
        SoundManager.PonerSonido("Fight");
        tiempo = 90f;
        J1.transform.position = new Vector2(-8, -0f);
            J1.GetComponent<Jugador>().TodoslosBooleansJ1();
            J2.transform.position = new Vector2(8, -0f);
            J2.GetComponent<Jugador2>().TodoslosBooleansJ2();

        }
    }

